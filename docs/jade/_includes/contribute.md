## Want to Contribute?

<p class="lead">Any release of UA Bootstrap is tested and ready to use. But improvements to the framework are in everyone's best interest.</p>

A team of web-focused volunteers known as UA Digital meets weekly to build and
test products like UA Bootstrap and [UA
Quickstart](https://brand.arizona.edu/guide/ua-quickstart).

### **Get Involved**

- Request an invite to the Friday UA Digital meetings by subscribing to the [UA
  Digital listserv](https://list.arizona.edu/sympa/info/ua-digital)
- Join the UA Digital discussions on
  [Slack](https://uarizona.slack.com/messages/ua-bootstrap)
- Submit pull requests on
  [Bitbucket](https://bitbucket.org/uadigital/ua-bootstrap)

Questions, bugs, or suggestions can also be emailed to
[brand@email.arizona.edu](mailto:brand@email.arizona.edu?subject=UA Bootstrap
Feedback')
